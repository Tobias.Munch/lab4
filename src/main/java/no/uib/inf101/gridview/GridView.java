package no.uib.inf101.gridview;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.IColorGrid;

import java.awt.geom.Rectangle2D;
import java.util.List;

public class GridView extends JPanel {
  IColorGrid iColorGrid;
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

  public GridView(IColorGrid iColorGrid) {
    this.iColorGrid = iColorGrid;
    this.setPreferredSize(new Dimension(400, 300));
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;

    drawGrid(g2);
  }

  private void drawGrid(Graphics2D g2) {
    double x = OUTERMARGIN;
    double y = OUTERMARGIN;
    double width = this.getWidth() - 2 * OUTERMARGIN;
    double height = this.getHeight() - 2 * OUTERMARGIN;
    g2.setColor(MARGINCOLOR);
    Rectangle2D box = new Rectangle2D.Double(x, y, width, height);
    g2.fill(box);

    CellPositionToPixelConverter cptpc = new CellPositionToPixelConverter(box, iColorGrid, OUTERMARGIN);

    drawCells(g2, iColorGrid, cptpc);
  }

  private static void drawCells(Graphics2D g2, CellColorCollection iColorGrid, CellPositionToPixelConverter cptpc) {
    List<CellColor> CellColors = iColorGrid.getCells();
    for (CellColor cellColor : CellColors) {
        CellPosition position = cellColor.cellPosition();
        Color color = cellColor.color();
        if (color == null) {
          color = Color.DARK_GRAY;
        }
        g2.setColor(color);
        Rectangle2D cell = cptpc.getBoundsForCell(position);
        g2.fill(cell);
    }
  }
}
